import axios from 'axios'

const pokemonSpecies = (params) => axios.get(params.api)

export const apiPokemonSpecies = ({ api }) => pokemonSpecies({ api })