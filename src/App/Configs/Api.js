// BASE POKEMON
export const BASE_POKEMON_API = `http://localhost:3000/api/`

// POKEMON LIST
export const POKEMON_LIST_API = `${BASE_POKEMON_API}/main.json`