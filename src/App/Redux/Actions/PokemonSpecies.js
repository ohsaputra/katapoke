import { createAction } from 'redux-actions'
import { REQ_POKEMON_SPECIES } from './Types'

export const reqPokemonSpecies = createAction(REQ_POKEMON_SPECIES.DEFAULT)