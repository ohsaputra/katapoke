import { put, all, call } from 'redux-saga/effects'
import {
  REQ_POKEMON_LIST,
  REQ_POKEMON,
  REQ_POKEMON_SPECIES,
} from '../Redux/Actions/Types'
import { apiPokemonList } from '../Services/Apis/PokemonList'
import { apiPokemon } from '../Services/Apis/Pokemon'
import { apiPokemonSpecies } from '../Services/Apis/PokemonSpecies'

export function* pokemonList(action) {
  try {
    const { payload } = action
    const data = yield apiPokemonList(payload)
    const pokemon = yield all(data.data.results.map(item => call(apiPokemon, { api: item.url})))
    const pokemonSpecies = yield all(pokemon.map(item => call(apiPokemonSpecies, {api: item.data.species.url})))

    yield put({ type: REQ_POKEMON_LIST.SUCCESS, data })
    yield put({ type: REQ_POKEMON.SUCCESS, data: pokemon })
    yield put({ type: REQ_POKEMON_SPECIES.SUCCESS, data: pokemonSpecies })
  } catch (e) {
    yield put({ type: REQ_POKEMON_LIST.FAILURE, errData: e })
  }
}