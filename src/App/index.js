import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './Redux/ConfigureStore'
import RouterConfig from './Navigations'

const store = configureStore()

const App = () => (
  <Provider store={store}>
    { RouterConfig }
  </Provider>
)

export default App