import React, { PureComponent } from 'react'
import { Header, Container } from 'semantic-ui-react'

import './header.css'

class MainHeader extends PureComponent {
  render() {
    return (
      <div className='header__container'>
        <Container>
          <Header as='h1'>POKEPEDIA</Header>
        </Container>
      </div>
    )
  }
}

export default MainHeader