import { put, all, call } from 'redux-saga/effects'
import { REQ_POKEMON } from '../Redux/Actions/Types'
import { apiPokemon } from '../Services/Apis/Pokemon'

export function* pokemon(action) {
  try {
    const { payload } = action
    const data = yield all(payload.data.map(item => call(apiPokemon, { api: item.url})))

    yield put({ type: REQ_POKEMON.SUCCESS, data })
  } catch (e) {
    yield put({ type: REQ_POKEMON.FAILURE, errData: e })
  }
}