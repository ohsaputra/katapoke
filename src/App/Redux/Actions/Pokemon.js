import { createAction } from 'redux-actions'
import { REQ_POKEMON } from './Types'

export const reqPokemon = createAction(REQ_POKEMON.DEFAULT)