import { put, all, call } from 'redux-saga/effects'
import { REQ_POKEMON_SPECIES } from '../Redux/Actions/Types'
import { apiPokemonSpecies } from '../Services/Apis/PokemonSpecies'

export function* pokemonSpecies(action) {
  try {
    const { payload } = action
    const data = yield all(payload.data.map(item => call(apiPokemonSpecies, {api: item.data.species.url})))

  yield put({ type: REQ_POKEMON_SPECIES.SUCCESS, data })
  } catch (e) {
  yield put({ type: REQ_POKEMON_SPECIES.FAILURE, errData: e })
  }
}