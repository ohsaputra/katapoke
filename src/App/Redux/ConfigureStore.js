import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducers from './Reducers'
import watchSagas from '../Sagas'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
  const store = createStore(rootReducers, applyMiddleware(sagaMiddleware))
  sagaMiddleware.run(watchSagas)

  return store
}
