import { createAction } from 'redux-actions'
import { REQ_POKEMON_LIST } from './Types'

export const reqPokemonList = createAction(REQ_POKEMON_LIST.DEFAULT)