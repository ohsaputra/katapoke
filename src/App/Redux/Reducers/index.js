import { combineReducers } from 'redux'

const rootReducers = combineReducers({
  pokemon: require('./Pokemon').reducer,
  pokemonList: require('./PokemonList').reducer,
  pokemonSpecies: require('./PokemonSpecies').reducer,
})

export default rootReducers