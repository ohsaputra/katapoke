import axios from 'axios'
import { POKEMON_LIST_API } from '../../Configs/Api'

let API = undefined

const pokemonList = (params) => {
  if (!params.api)
    API = POKEMON_LIST_API
  else
    API = params.api

  return axios.get(API)
}

export const apiPokemonList = ({ api }) => pokemonList({ api })