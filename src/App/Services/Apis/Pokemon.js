import axios from 'axios'

const pokemon = (params) => axios.get(params.api)

export const apiPokemon = ({ api }) => pokemon({ api })