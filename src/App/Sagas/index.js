import * as Types from '../Redux/Actions/Types'
import { all, takeEvery } from 'redux-saga/effects'

import { pokemonList } from './PokemonList'
import { pokemon } from './Pokemon'
import { pokemonSpecies } from './PokemonSpecies'

export default function* watchSagas() {
  yield all([
    yield takeEvery(Types.REQ_POKEMON.DEFAULT, pokemon),
    yield takeEvery(Types.REQ_POKEMON_LIST.DEFAULT, pokemonList),
    yield takeEvery(Types.REQ_POKEMON_SPECIES.DEFAULT, pokemonSpecies),
  ])
}