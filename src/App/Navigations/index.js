import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Main from '../Components/Scenes/Main'

const RouterConfig = (
  <Router>
    <div>
      <Switch>
        <Route exact path='/' component={Main} />
      </Switch>
    </div>
  </Router>
)

export default RouterConfig