import { REQ_POKEMON } from '../Actions/Types'

export const INITIAL_STATE = {
  data: null,
  isFetching: false,
  error: false
}

export const request = (state, action) => {
  return {
    ...state,
    data: null,
    isFetching: true
  }
}

export const success = (state, action) => {
  return {
    ...state,
    data: action.data,
    isFetching: false,
    error: false
  }
}

export const failure = (state, action) => {
  return {
    ...state,
    data: action.errData,
    isFetching: false,
    error: true
  }
}

export const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQ_POKEMON.DEFAULT:
      return request(state, action)
    case REQ_POKEMON.SUCCESS:
      return success(state, action)
    case REQ_POKEMON.FAILURE:
      return failure(state, action)
    default:
      return state
  }
}