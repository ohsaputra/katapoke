// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Container,
  Grid,
  Image,
  Modal,
  Button
} from 'semantic-ui-react'

import { reqPokemonList } from '../../../Redux/Actions/PokemonList'
import MainHeader from '../../Presentationals/Header'
import './home.css'

type OwnProps = {}

type StateProps = {
  pokemonList: Object,
  pokemon: Object,
  pokemonSpecies: Object
}

type DispatchProps = {
  reqPokemonList: (any: Object) => void,
}

type Props = OwnProps & StateProps & DispatchProps

type State = {
  pokemon: Object,
  pokemonList: Object,
  pokemonSpecies: Object,
  open: boolean,
  id: number | null
}

class Main extends Component<Props, State> {

  state = {
    pokemon: {},
    pokemonList: {},
    pokemonSpecies: {},
    open: false,
    id: null
  }

  render() {

    const {
      pokemon,
      pokemonList,
      pokemonSpecies,
      open,
      id
    } = this.state

    return (
      <div className="App">
        <MainHeader />

        <Container>
          <Grid columns={5}>
            <Grid.Row>
              { this.renderPokemon() }
            </Grid.Row>
          </Grid>

          { pokemonList.previous &&
            <a
              onClick={(e) => this.handleNavigation(pokemonList.previous, e)}
              className='pokemon__navigation pokemon__navigation--left'>Prev</a>
          }

          { pokemonList.next &&
            <a
              onClick={(e) => this.handleNavigation(pokemonList.next, e)}
              className='pokemon__navigation pokemon__navigation--right'>Next</a>
          }
        </Container>

        { id !== null &&
          <Modal size='tiny' open={open} onClose={this.close} >
            <Modal.Content className='pokemon__modal'>
            <div className='pokemon__modal_head'>
              <Image className='pokemon__image' src={ pokemon[id].data.sprites.front_default } />
              <div className='pokemon__heading_name pokemon__heading_name--bold'>{ pokemon[id].data.name}</div>
            </div>
            <div className='pokemon__modal_description'>
              <div className='pokemon__modal_item'>
                <p className='pokemon__heading_name'>Description</p>
                <p>
                  {
                    pokemonSpecies[id].data.flavor_text_entries[1].language.name !== 'en'
                    ? pokemonSpecies[id].data.flavor_text_entries[2].flavor_text
                    : pokemonSpecies[id].data.flavor_text_entries[1].flavor_text
                  }
                </p>
              </div>
              <Grid columns={4}>
                <Grid.Row>
                  <Grid.Column>
                    <div className='pokemon__modal_item'>
                      <p className='pokemon__heading_name'>Color</p>
                      <p>{ pokemonSpecies[id].data.color.name}</p>
                    </div>
                  </Grid.Column>
                  <Grid.Column>
                    <div className='pokemon__modal_item'>
                      <p className='pokemon__heading_name'>Shapes</p>
                      <p>{ pokemonSpecies[id].data.shape.name}</p>
                    </div>
                  </Grid.Column>
                  <Grid.Column>
                    <div className='pokemon__modal_item'>
                      <p className='pokemon__heading_name'>Weight</p>
                      <p>{ pokemon[id].data.weight} kg</p>
                    </div>
                  </Grid.Column>
                  <Grid.Column>
                    <div className='pokemon__modal_item'>
                      <p className='pokemon__heading_name'>Height</p>
                      <p>{ pokemon[id].data.height} cm</p>
                    </div>
                  </Grid.Column>
                  <Grid.Column>
                    <div className='pokemon__modal_item'>
                      <p className='pokemon__heading_name'>Habitat</p>
                      <p>{ pokemonSpecies[id].data.habitat.name}</p>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <div style={{textAlign: 'right'}}>
                <Button onClick={this.close}>Close</Button>
              </div>
            </div>
            </Modal.Content>
          </Modal>
        }


      </div>
    )
  }

  componentDidMount() {
    this.props.reqPokemonList({ api: '' })
  }

  componentWillReceiveProps(props) {
    const { pokemonList, pokemon, pokemonSpecies } = props

    if (!pokemonList.error && pokemonList.data) {
      this.setState({
        pokemonList: pokemonList.data.data,
        pokemon: pokemon.data,
        pokemonSpecies: pokemonSpecies.data
      })
    }
  }

  renderPokemon = () => {
    const {
      pokemon,
      pokemonList,
      pokemonSpecies,
    } = this.state

    if (pokemonList.results)
      return pokemonList.results.map((poke, key) => (
        <Grid.Column className='pokemon' key={key}>
          <a onClick={e => this.show(key, e)}>
            <Image className='pokemon__image' src={
              pokemon !== null
              ? pokemon[key].data.sprites.front_default
              : ''
            } />
            <div className='pokemon__heading_name pokemon__heading_name--bold'>{poke.name}</div>
            <div className='pokemon__heading_name'>
            {
              pokemonSpecies !== null
              ? pokemonSpecies[key].data.habitat.name
              : ''
            }
            </div>
          </a>
        </Grid.Column>
      ))

    return
  }

  show = (key, e) => {
    e.preventDefault()
    this.setState({id: key, open: true })
  }
  close = () => this.setState({ open: false })
  handleNavigation = (api, e) => {
    e.preventDefault()
    this.props.reqPokemonList({ api })
  }

}

const mapStateToProps = (state: Object): StateProps => ({
  pokemon: state.pokemon,
  pokemonList: state.pokemonList,
  pokemonSpecies: state.pokemonSpecies,
})

const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  reqPokemonList: (payload) => dispatch(reqPokemonList(payload)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main)